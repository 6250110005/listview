import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    NetworkImage('https://png.pngtree.com/png-clipart/20190604/original/pngtree-particle-artcyclists-png-image_932358.jpg'),
    NetworkImage(
        'https://i.pinimg.com/originals/97/a5/10/97a5103a7a5f3edc32657e48bf3bb775.jpg'),
    NetworkImage(
        'https://png.pngtree.com/png-clipart/20190604/original/pngtree-long-distance-bus-bus-car-png-image_1368170.jpg'),
    NetworkImage(
        'https://e1.pngegg.com/pngimages/777/883/png-clipart-voiture-de-dessin-anime-dessin-anime-voiture-electrique-dessin-vehicule-porte-de-vehicule-technologie-voiture-miniature.png'),
    NetworkImage(
        'https://e1.pngegg.com/pngimages/917/433/png-clipart-train-maglev-dessin-dessin-anime-gratis-levitation-magnetique-train-a-grande-vitesse-jaune.png'),
    NetworkImage('https://e7.pngegg.com/pngimages/486/303/png-clipart-military-cadence-computer-icons-icon-design-others-hand-logo.png'),
    NetworkImage(
        'https://png.pngtree.com/element_our/20200703/ourlarge/pngtree-subway-travel-railroad-train-image_2301437.jpg'),
    NetworkImage(
        'https://is5-ssl.mzstatic.com/image/thumb/Purple113/v4/3b/98/ef/3b98eff3-2cef-dcd5-4d91-f80fcdc98855/source/512x512bb.jpg'),
    NetworkImage(
        'https://img.lovepik.com/free_png/32/60/68/28558PICEckVc6fB4WhfG_PIC2018.png_860.png'),
  ];

  // final icons = [
  //   Icons.directions_bike,
  //   Icons.directions_boat,
  //   Icons.directions_bus,
  //   Icons.directions_car,
  //   Icons.directions_railway,
  //   Icons.directions_run,
  //   Icons.directions_subway,
  //   Icons.directions_transit,
  //   Icons.directions_walk
  // ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('List View'),
          ),
          body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: images[index],
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'There are many passengers in serveral vehicles',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text(
                            'Welcome'), // To display the title it is optional
                        content: Text(
                            'This is a ${titles[index]}'), // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button

                            onPressed: () {
                              Navigator.of(context).pop();
                            }, // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          )),
    );
  }
}
